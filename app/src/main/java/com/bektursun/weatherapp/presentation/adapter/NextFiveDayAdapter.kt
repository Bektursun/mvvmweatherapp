package com.bektursun.weatherapp.presentation.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.weatherapp.R
import com.bektursun.weatherapp.presentation.FutureWeather
import kotlinx.android.synthetic.main.item_forecast.view.*

class NextFiveDayAdapter(
    private val context: Context?,
    private val weather: List<FutureWeather>
): RecyclerView.Adapter<NextFiveDayAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_current_weather_details_card, parent, false))
    }

    override fun getItemCount(): Int {
        return weather.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(weather[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(weatherDay: FutureWeather){
            itemView.day.text = weatherDay.message.toString()
            itemView.forecast_temperature.text = weatherDay.cod
        }
    }
}