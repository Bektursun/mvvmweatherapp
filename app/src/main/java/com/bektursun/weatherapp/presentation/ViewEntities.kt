package com.bektursun.weatherapp.presentation

import androidx.annotation.StringRes
import com.bektursun.weatherapp.repository.net.City
import com.bektursun.weatherapp.repository.net.X


data class CityCurrentWeather(
    val cityId: String,
    val cityName: String,
    val temperature: Int,
    val windSpeed: Double,
    val airHumidity: Int,
    @StringRes val conditionStringRes: Int,
    val pressure: Int,
    val weatherIcon: String
)

// FutureForecast
data class FutureWeather(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<X>,
    val message: Double
)

data class ApiError(
    val errorType: ErrorType,
    @StringRes val messageStringRes: Int
)

enum class ErrorType {
    HTTP_ERROR, NETWORK_ERROR, UNKNOWN
}
