package com.bektursun.weatherapp.presentation.city_weather_details

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bektursun.weatherapp.R
import com.bektursun.weatherapp.presentation.CityCurrentWeather
import com.bektursun.weatherapp.presentation.FutureWeather
import com.bektursun.weatherapp.presentation.city_weather_details.CityWeatherDetailsViewModel.CityWeatherDetailsState.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.lang.IllegalStateException
import com.bektursun.weatherapp.presentation.city_weather_details.CityWeatherDetailsViewModel.*
import com.bektursun.weatherapp.utils.gone
import com.bektursun.weatherapp.utils.visible
import kotlinx.android.synthetic.main.fragment_city_weather_details.*
import kotlinx.android.synthetic.main.item_forecast.*
import kotlinx.android.synthetic.main.item_forecast2.*
import kotlinx.android.synthetic.main.item_forecast3.*
import kotlinx.android.synthetic.main.item_forecast4.*
import kotlinx.android.synthetic.main.item_forecast5.*
import kotlinx.android.synthetic.main.layout_current_weather_details_card.*
import kotlinx.android.synthetic.main.layout_error_view.*
import kotlinx.android.synthetic.main.layout_error_view.view.*
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

private const val CITY_ID_BUNDLE_EXTRA = "CITY_ID"

class CityWeatherDetailsFragment : Fragment() {

    companion object {
        fun createBundle(cityId: String) = Bundle().apply {
            putString(CITY_ID_BUNDLE_EXTRA, cityId)
        }
    }

    private lateinit var cityId: String
    private val viewModel: CityWeatherDetailsViewModel by viewModel { parametersOf(cityId) }
    private val futureViewModel: FutureCityWeatherDetailsViewModel by viewModel { parametersOf(cityId)}

    // Observer - Called when the data is changed.
    // Подпсчиками LiveData являются activity and fragment
    // @param t  The new data
    private val weatherStateObserver = Observer<CityWeatherDetailsState> { handleWeatherState(it) }
    private val futureWeatherStateObserver = Observer<FutureCityWeatherDetailsViewModel.CityFutureWeatherDetailsState> { handleFutureWeatherState(it) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_city_weather_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cityId = arguments?.getString(CITY_ID_BUNDLE_EXTRA)
            ?: throw IllegalStateException("City ID must be provided to this fragment")
        viewModel.loadCityWeatherDetails()
        futureViewModel.loadFutureCityWeatherDetails()
        closeBtn.setOnClickListener { findNavController().navigateUp() }
        weatherDataContainer.setOnRefreshListener { viewModel.loadCityWeatherDetails() }
        weatherDataContainer.setOnRefreshListener { futureViewModel.loadFutureCityWeatherDetails() }
    }

    override fun onResume() {
        super.onResume()
        // observe наблюдать
        // weatherStateObserver - наблюдатель погодного состояния
        viewModel.cityWeatherDetailsState.observe(viewLifecycleOwner, weatherStateObserver)
        futureViewModel.cityFutureWeatherDetailsState.observe(viewLifecycleOwner, futureWeatherStateObserver)
    }

    private fun handleWeatherState(state: CityWeatherDetailsState) = when (state) {
        Loading -> showLoading()
        FutureLoading -> showLoading()
        is LoadingSuccess -> showWeatherData(state.currentWeather)
        is Error -> showErrorView(state)
    }

    private fun handleFutureWeatherState(state: FutureCityWeatherDetailsViewModel.CityFutureWeatherDetailsState) = when (state) {
        FutureCityWeatherDetailsViewModel.CityFutureWeatherDetailsState.fLoading -> showLoading()
        FutureCityWeatherDetailsViewModel.CityFutureWeatherDetailsState.fFutureLoading -> showLoading()
        is FutureCityWeatherDetailsViewModel.CityFutureWeatherDetailsState.fLoadingSuccessForFuture -> showFutureWeatherData(state.futureWeather)
        is FutureCityWeatherDetailsViewModel.CityFutureWeatherDetailsState.fError -> showFutureErrorView(state)
    }

    private fun showLoading() {
        weatherDataContainer.isRefreshing = true
        errorView.gone()
    }

    private fun showWeatherData(currentWeather: CityCurrentWeather) {
        weatherDataContainer.visible()
        weatherDataContainer.isRefreshing = false
        errorView.gone()
        updateCurrentWeatherData(currentWeather)
    }

    private fun showFutureWeatherData(futureWeather: FutureWeather) {
        Log.d("Future", futureWeather.toString())
        updateFutureWeatherData(futureWeather)
    }

    private fun showErrorView(state: Error) {
        weatherDataContainer.isRefreshing = false
        weatherDataContainer.gone()
        errorView.visible()
        errorView.errorMessage.text = getString(state.messageRes)
        tryAgainBtn.setOnClickListener { viewModel.loadCityWeatherDetails() }
    }

    private fun showFutureErrorView(state: FutureCityWeatherDetailsViewModel.CityFutureWeatherDetailsState.fError) {
        weatherDataContainer.isRefreshing = false
        weatherDataContainer.gone()
        errorView.visible()
        errorView.errorMessage.text = getString(state.messageRes)
        tryAgainBtn.setOnClickListener { futureViewModel.loadFutureCityWeatherDetails() }
    }

    private fun updateCurrentWeatherData(currentWeather: CityCurrentWeather) = with(currentWeather) {
        city.text = cityName
        temperatureValue.text = getString(R.string.celsius_temperature, temperature)
        currentWeatherIcon.setAnimation(weatherIcon)
        pressureValue.text = getString(R.string.h_pascal_pressure, pressure)
        currentWeatherDescription.text = getString(conditionStringRes).capitalize()
        humidityValue.text = getString(R.string.humidity_percentage, airHumidity)
        windSpeedValue.text = getString(R.string.wind_speed_meters_per_second, windSpeed)
    }

    private fun updateFutureWeatherData(futureWeather: FutureWeather) = with(futureWeather) {
        convert(listOf(futureWeather))
    }
    private fun convert(weather: List<FutureWeather>) = weather.map {
        weather.forEach {

            val dayOne = String.format(Locale.ENGLISH, "%tA", it.list[0].dt * 1000L).substring(0, 3).toUpperCase() //toDay(it.list[0].dt.toLong())
            val dayss = String.format(Locale.ENGLISH, "%tA", it.list[8].dt * 1000L).substring(0, 3).toUpperCase()
            val secondDay =  String.format(Locale.ENGLISH, "%tA", it.list[17].dt * 1000L).substring(0, 3).toUpperCase() //toDay(it.list[17].dt.toLong())
            val thirdDay =  String.format(Locale.ENGLISH, "%tA", it.list[26].dt * 1000L).substring(0, 3).toUpperCase() //toDay(it.list[26].dt.toLong())
            val fourthDay = String.format(Locale.ENGLISH, "%tA", it.list[35].dt * 1000L).substring(0, 3).toUpperCase() //toDay(it.list[35].dt.toLong())
            day.text = dayOne
            day2.text = dayss
            day3.text = secondDay
            day4.text = thirdDay
            day5.text = fourthDay

            forecast_temperature.text = getString(R.string.celsius_temperature, it.list[0].main.tempMax.toInt())
            forecast_temperature2.text = getString(R.string.celsius_temperature,it.list[8].main.tempMax.toInt())
            forecast_temperature3.text = getString(R.string.celsius_temperature, it.list[17].main.tempMax.toInt())
            forecast_temperature4.text = getString(R.string.celsius_temperature, it.list[26].main.tempMax.toInt())
            forecast_temperature5.text = getString(R.string.celsius_temperature, it.list[35].main.tempMax.toInt())
        }

    }
    @SuppressLint("NewApi")
    private fun toDay(datetime: Long): String{
        val formatter = DateTimeFormatter.ofPattern("dd MMMM")
        return Instant.ofEpochSecond(datetime).atZone(ZoneId.of("GMT-4")).format(formatter)
    }


}