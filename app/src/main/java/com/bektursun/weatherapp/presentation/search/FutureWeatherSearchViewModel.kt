package com.bektursun.weatherapp.presentation.search

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hadilq.liveevent.LiveEvent
import com.bektursun.weatherapp.domain.error.MapErrorUseCase
import com.bektursun.weatherapp.domain.future_weather.GetFutureWeatherUseCase
import com.bektursun.weatherapp.domain.search_history.GetLastSearchCityIdFutureForecastUseCase
import com.bektursun.weatherapp.domain.search_history.SaveLastSearchFutureCityIdUseCase
import com.bektursun.weatherapp.presentation.FutureWeather
import io.reactivex.Maybe
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FutureWeatherSearchViewModel(
    private val getFutureWeatherUseCase: GetFutureWeatherUseCase,
    private val getLastSearchCityIdFutureUseCase: GetLastSearchCityIdFutureForecastUseCase,
    private val saveLastSearchFutureCityIdUseCase: SaveLastSearchFutureCityIdUseCase,
    private val mapErrorUseCase: MapErrorUseCase
): ViewModel() {

    val futureWeatherSearchState = MutableLiveData<FutureWeatherSearchState>()
    val lastSearchFutureState = MutableLiveData<FutureWeather>()
    val futureResultEvent = LiveEvent<FutureWeather?>()

    private var futureSearchDisposable: Disposable? = null
    private var lastFutureSearchDisposable: Disposable? = null

    fun loadFutureDataForCityName(cityName: String) {
        futureWeatherSearchState.postValue(FutureWeatherSearchState.fLoading)
        futureSearchDisposable?.dispose()
        futureSearchDisposable = getFutureWeatherUseCase.executeFutureForCityName(cityName)
            .subscribeOn(Schedulers.io())
            .flatMapCompletable {
                futureWeatherSearchState.postValue(FutureWeatherSearchState.fIdle)
                futureResultEvent.postValue(it)
                saveLastSearchFutureCityIdUseCase.executeForFuture(it.city.id.toString())
            }
            .subscribe({}, ::handleFutureWeatherSearchError)
    }

    fun loadFutureDataForLocation(latitude: Double, longitude: Double) {
        futureWeatherSearchState.postValue(FutureWeatherSearchState.fLoading)
        futureSearchDisposable?.dispose()
        futureSearchDisposable = getFutureWeatherUseCase.executeFutureForLocation(latitude, longitude)
            .subscribeOn(Schedulers.io())
            .flatMapCompletable {
                futureWeatherSearchState.postValue(FutureWeatherSearchState.fIdle)
                futureResultEvent.postValue(it)
                saveLastSearchFutureCityIdUseCase.executeForFuture(it.city.id.toString())
            }
            .subscribe({}, ::handleFutureWeatherSearchError)
    }

    fun loadLastFutureCityForecast() {
        lastFutureSearchDisposable = getLastSearchCityIdFutureUseCase.executeFuture()
            .subscribeOn(Schedulers.io())
            .flatMapMaybe {
                if (it.isNotEmpty()) getFutureWeatherUseCase.executeFutureForCityId(it).toMaybe()
                else Maybe.empty()
            }
            .subscribe(
                { lastSearchFutureState.postValue(it) },
                { it.printStackTrace() }
            )
    }

    private fun handleFutureWeatherSearchError(throwable: Throwable) {
        val apiError = mapErrorUseCase.execute(throwable)
        futureWeatherSearchState.postValue(FutureWeatherSearchState.fError(apiError.messageStringRes))
    }

    override fun onCleared() {
        super.onCleared()
        futureSearchDisposable?.dispose()
        lastFutureSearchDisposable?.dispose()
    }

    sealed class FutureWeatherSearchState {
        object fLoading : FutureWeatherSearchState()
        data class fError(@StringRes val messageRes: Int) : FutureWeatherSearchState()
        object fIdle : FutureWeatherSearchState()
    }
}