package com.bektursun.weatherapp.presentation.search

import androidx.annotation.StringRes
import androidx.lifecycle.*
import com.hadilq.liveevent.LiveEvent
import com.bektursun.weatherapp.domain.search_history.GetLastSearchCityIdUseCase
import com.bektursun.weatherapp.domain.current_weather.GetCurrentWeatherUseCase
import com.bektursun.weatherapp.domain.error.MapErrorUseCase
import com.bektursun.weatherapp.domain.search_history.SaveLastSearchCityIdUseCase
import com.bektursun.weatherapp.presentation.CityCurrentWeather
import io.reactivex.Maybe
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class WeatherSearchViewModel(
    private val getCurrentWeatherUseCase: GetCurrentWeatherUseCase,
    //private val getFutureWeatherUseCase: GetFutureWeatherUseCase,
    private val getLastSearchCityIdUseCase: GetLastSearchCityIdUseCase,
    //private val getLastSearchCityIdFutureUseCase: GetLastSearchCityIdFutureForecastUseCase,
    private val saveLastSearchCityIdUseCase: SaveLastSearchCityIdUseCase,
    //private val saveLastSearchFutureCityIdUseCase: SaveLastSearchFutureCityIdUseCase,
    private val mapErrorUseCase: MapErrorUseCase
) : ViewModel() {

    val weatherSearchState = MutableLiveData<WeatherSearchState>()
    //val futureWeatherSearchState = MutableLiveData<FutureWeatherSearchState>()
    val lastSearchState = MutableLiveData<CityCurrentWeather>()
    //val lastSearchFutureState = MutableLiveData<FutureWeather>()
    val searchResultEvent = LiveEvent<CityCurrentWeather?>()
    //val futureResultEvent = LiveEvent<FutureWeather?>()

    /*
    * Применительно к Observable тип Disposable позволяет вызывать метод dispose,
    * означающий «Я закончил работать с этим ресурсом, мне больше не нужны данные».
    * Если у вас есть сетевой запрос, то он может быть отменён.
    * Если вы прослушивали бесконечный поток нажатий кнопок, то это будет означать,
    * что вы больше не хотите получать эти события, в таком случае можно удалить OnClickListener у View.*/
    private var searchDisposable: Disposable? = null
    //private var futureSearchDisposable: Disposable? = null
    private var lastSearchDisposable: Disposable? = null
    //private var lastFutureSearchDisposable: Disposable? = null

    fun loadDataForCityName(cityName: String) {
        weatherSearchState.postValue(WeatherSearchState.Loading)
        searchDisposable?.dispose()
        searchDisposable = getCurrentWeatherUseCase.executeForCityName(cityName)
            .subscribeOn(Schedulers.io())
            .flatMapCompletable {
                weatherSearchState.postValue(WeatherSearchState.Idle)
                searchResultEvent.postValue(it)
                saveLastSearchCityIdUseCase.execute(it.cityId)
            }
            .subscribe({}, ::handleWeatherSearchError)
    }

   /* fun loadFutureDataForCityName(cityName: String) {
        futureWeatherSearchState.postValue(FutureWeatherSearchState.fLoading)
        futureSearchDisposable?.dispose()
        futureSearchDisposable = getFutureWeatherUseCase.executeFutureForCityName(cityName)
            .subscribeOn(Schedulers.io())
            .flatMapCompletable {
                futureWeatherSearchState.postValue(FutureWeatherSearchState.fIdle)
                futureResultEvent.postValue(it)
                saveLastSearchFutureCityIdUseCase.executeForFuture(it.id)
            }
            .subscribe({}, ::handleFutureWeatherSearchError)
    }*/

    fun loadDataForLocation(latitude: Double, longitude: Double) {
        weatherSearchState.postValue(WeatherSearchState.Loading)
        searchDisposable?.dispose()
        searchDisposable = getCurrentWeatherUseCase.executeForLocation(latitude, longitude)
            .subscribeOn(Schedulers.io())
            .flatMapCompletable {
                weatherSearchState.postValue(WeatherSearchState.Idle)
                searchResultEvent.postValue(it)
                saveLastSearchCityIdUseCase.execute(it.cityId)
            }
            .subscribe({}, ::handleWeatherSearchError)
    }

    /*fun loadFutureDataForLocation(latitude: Double, longitude: Double) {
        futureWeatherSearchState.postValue(FutureWeatherSearchState.fLoading)
        futureSearchDisposable?.dispose()
        futureSearchDisposable = getFutureWeatherUseCase.executeFutureForLocation(latitude, longitude)
            .subscribeOn(Schedulers.io())
            .flatMapCompletable {
                futureWeatherSearchState.postValue(FutureWeatherSearchState.fIdle)
                futureResultEvent.postValue(it)
                saveLastSearchFutureCityIdUseCase.executeForFuture(it.id)
            }
            .subscribe({}, ::handleFutureWeatherSearchError)
    }*/

    fun loadLastCityForecast() {
        lastSearchDisposable = getLastSearchCityIdUseCase.execute()
            .subscribeOn(Schedulers.io())
            .flatMapMaybe {
                if (it.isNotEmpty()) getCurrentWeatherUseCase.executeForCityId(it).toMaybe()
                else Maybe.empty()
            }
            .subscribe(
                { lastSearchState.postValue(it) },
                { it.printStackTrace() }
            )
    }

    /*fun loadLastFutureCityForecast() {
        lastFutureSearchDisposable = getLastSearchCityIdFutureUseCase.executeFuture()
            .subscribeOn(Schedulers.io())
            .flatMapMaybe {
                if (it.isNotEmpty()) getFutureWeatherUseCase.executeFutureForCityId(it).toMaybe()
                else Maybe.empty()
            }
            .subscribe(
                { lastSearchFutureState.postValue(it) },
                { it.printStackTrace() }
            )
    }*/

    private fun handleWeatherSearchError(throwable: Throwable) {
        val apiError = mapErrorUseCase.execute(throwable)
        weatherSearchState.postValue(WeatherSearchState.Error(apiError.messageStringRes))
    }

    /*private fun handleFutureWeatherSearchError(throwable: Throwable) {
        val apiError = mapErrorUseCase.execute(throwable)
        futureWeatherSearchState.postValue(FutureWeatherSearchState.fError(apiError.messageStringRes))
    }*/

    override fun onCleared() {
        super.onCleared()
        searchDisposable?.dispose()
        lastSearchDisposable?.dispose()
        //futureSearchDisposable?.dispose()
        //lastFutureSearchDisposable?.dispose()
    }

    sealed class WeatherSearchState {
        object Loading : WeatherSearchState()
        data class Error(@StringRes val messageRes: Int) : WeatherSearchState()
        object Idle : WeatherSearchState()
    }

   /* sealed class FutureWeatherSearchState {
        object fLoading : FutureWeatherSearchState()
        data class fError(@StringRes val messageRes: Int) : FutureWeatherSearchState()
        object fIdle : FutureWeatherSearchState()
    }*/

}