package com.bektursun.weatherapp.presentation.city_weather_details

import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bektursun.weatherapp.domain.ForecastWeather
import com.bektursun.weatherapp.domain.error.MapErrorUseCase
import com.bektursun.weatherapp.domain.future_weather.GetFutureWeatherUseCase
import com.bektursun.weatherapp.presentation.FutureWeather
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FutureCityWeatherDetailsViewModel(
    private val cityId: String,
    private val getFutureWeatherUseCase: GetFutureWeatherUseCase,
    private val mapErrorUseCase: MapErrorUseCase
): ViewModel() {

    val cityFutureWeatherDetailsState = MutableLiveData<CityFutureWeatherDetailsState>()
    var forecastWeather: LiveData<List<ForecastWeather>> = MutableLiveData()
    private var futureDisposable: Disposable? = null

    fun loadFutureCityWeatherDetails() {
        futureDisposable?.dispose()
        futureDisposable = getFutureWeatherUseCase.executeFutureForCityId(cityId)
            .doOnSubscribe { cityFutureWeatherDetailsState.postValue(CityFutureWeatherDetailsState.fLoading) }
            .subscribeOn(Schedulers.io())
            .subscribe({ cityFutureWeatherDetailsState.postValue(CityFutureWeatherDetailsState.fLoadingSuccessForFuture(it)) }, ::handleFutureError)
    }

    private fun handleFutureError(throwable: Throwable) {
        val error = mapErrorUseCase.execute(throwable)
        Log.d("Future", throwable.localizedMessage)
        cityFutureWeatherDetailsState.postValue(CityFutureWeatherDetailsState.fError(error.messageStringRes))
    }

    override fun onCleared() {
        super.onCleared()
        futureDisposable?.dispose()
    }

    sealed class CityFutureWeatherDetailsState {
        object fLoading : CityFutureWeatherDetailsState()
        object fFutureLoading: CityFutureWeatherDetailsState()
        data class fLoadingSuccessForFuture(val futureWeather: FutureWeather): CityFutureWeatherDetailsState()
        data class fError(@StringRes val messageRes: Int) : CityFutureWeatherDetailsState()
    }
}