package com.bektursun.weatherapp.repository

import com.bektursun.weatherapp.utils.IConverter

class NextFiveDayWeather(): IConverter {

    var description: String? = null
    var dayDate: String? = null
    var dayHour: String? = null
    var celsius: Int? = null

    constructor(datetime: Long, description: String) : this(){

        this.description = description
        this.dayHour = toHour(datetime)
        this.dayDate = toDay(datetime)
        this.celsius = celsius
    }
}