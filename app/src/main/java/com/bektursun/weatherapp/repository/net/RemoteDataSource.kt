package com.bektursun.weatherapp.repository.net

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface RemoteDataSource {

    @GET("weather")
    //  Напомню, что в Single может прийти только один onNext, либо OnError. После этого Single считается завершенным.
    fun getWeatherForecast(@QueryMap queryMap: Map<String, String>): Single<WeatherForecast>

}