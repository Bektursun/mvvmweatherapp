package com.bektursun.weatherapp.repository.net

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlin.collections.List

// Мы аннотировали класс, с помощью @JsonClass(generateAdapter = true)которого будет сгенерирован JsonAdapter для
// обработки сериализации / десериализации в и из JSON указанного типа.
@JsonClass(generateAdapter = true)
data class WeatherForecast(
    // @Json(name = “value”)Аннотаций определяет имя ключа JSON для сериализации и свойства , чтобы установить значение на с десериализацией.
    @Json(name = "id") val cityId: String,
    @Json(name = "name") val cityName: String,
    val weather: kotlin.collections.List<WeatherData>,
    val main: MainForecastInfo,
    val wind: WindInfo
)

data class MainForecastInfo(
    @Json(name = "temp") val temp: Double,
    val humidity: Int,
    val pressure: Int
)

data class WindInfo(val speed: Double)

@Json(name = "Weather")
data class WeatherData(
    val id: String,
    @Json(name = "main") val condition: WeatherCondition,
    val icon: String,
    val description: String
)
// future forecast data classes
@JsonClass(generateAdapter = true)
data class FutureWeatherForecast(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<X>,
    val message: Double
)

@JsonClass(generateAdapter = true)
data class City(
    val coord: Coord,
    val country: String,
    val id: Int,
    val name: String,
   // val population: Int,
    val timezone: Int
)

@JsonClass(generateAdapter = true)
data class X(
    val clouds: Clouds,
    val dt: Int,
    @Json(name = "dt_txt")
    val dtTxt: String,
    val main: Main,
    val sys: Sys,
    val weather: List<Weather>,
    val wind: Wind
)

@JsonClass(generateAdapter = true)
data class Coord(
    val lat: Double,
    val lon: Double
)

@JsonClass(generateAdapter = true)
data class Clouds(
    val all: Int
)

@JsonClass(generateAdapter = true)
data class Weather(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)

@JsonClass(generateAdapter = true)
data class Main(
    @Json(name = "grnd_level")
    val grndLevel: Double,
    val humidity: Int,
    val pressure: Double,
    @Json(name = "sea_level")
    val seaLevel: Double,
    val temp: Double,
    @Json(name = "temp_kf")
    val tempKf: Double,
    @Json(name = "temp_max")
    val tempMax: Double,
    @Json(name = "temp_min")
    val tempMin: Double
)

@JsonClass(generateAdapter = true)
data class Wind(
    val deg: Double,
    val speed: Double
)

@JsonClass(generateAdapter = true)
data class Sys(
    val pod: String
)
//
enum class WeatherCondition {
    @Json(name = "Thunderstorm")
    THUNDERSTORM,
    @Json(name = "Drizzle")
    DRIZZLE,
    @Json(name = "Rain")
    RAIN,
    @Json(name = "Snow")
    SNOW,
    @Json(name = "Mist")
    MIST,
    @Json(name = "Smoke")
    SMOKE,
    @Json(name = "Haze")
    HAZE,
    @Json(name = "Dust")
    DUST,
    @Json(name = "Fog")
    FOG,
    @Json(name = "Sand")
    SAND,
    @Json(name = "Ash")
    ASH,
    @Json(name = "Squall")
    SQUALL,
    @Json(name = "Tornado")
    TORNADO,
    @Json(name = "Clear")
    CLEAR,
    @Json(name = "Clouds")
    CLOUDS
}