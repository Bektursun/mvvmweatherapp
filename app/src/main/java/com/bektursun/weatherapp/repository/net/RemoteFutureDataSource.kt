package com.bektursun.weatherapp.repository.net

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface RemoteFutureDataSource {

    @GET("forecast")
    fun getFutureForecast(@QueryMap queryMap: Map<String, String>): Single<FutureWeatherForecast>

}