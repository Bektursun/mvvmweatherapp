package com.bektursun.weatherapp.repository.net.interceptors

import com.bektursun.weatherapp.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException


class QueryParamsInterceptor : Interceptor {

    // Перехватчики (Interceptors)
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        // запрос
        val original = chain.request()
        // original url
        val originalHttpUrl = original.url()

        val url = originalHttpUrl.newBuilder()
            .addQueryParameter("appid", BuildConfig.API_KEY)
            .addQueryParameter("units", "metric")
            .build()

        val requestBuilder = original.newBuilder().url(url)
        val request = requestBuilder.build()

        return chain.proceed(request)
    }

}