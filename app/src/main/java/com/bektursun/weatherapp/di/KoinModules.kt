package com.bektursun.weatherapp.di

import com.bektursun.weatherapp.domain.search_history.GetLastSearchCityIdUseCase
import com.bektursun.weatherapp.domain.current_weather.GetCurrentWeatherUseCase
import com.bektursun.weatherapp.domain.error.MapErrorUseCase
import com.bektursun.weatherapp.domain.future_weather.GetFutureWeatherUseCase
import com.bektursun.weatherapp.domain.search_history.GetLastSearchCityIdFutureForecastUseCase
import com.bektursun.weatherapp.domain.search_history.SaveLastSearchCityIdUseCase
import com.bektursun.weatherapp.domain.search_history.SaveLastSearchFutureCityIdUseCase
import com.bektursun.weatherapp.presentation.city_weather_details.CityWeatherDetailsViewModel
import com.bektursun.weatherapp.presentation.city_weather_details.FutureCityWeatherDetailsViewModel
import com.bektursun.weatherapp.presentation.search.FutureWeatherSearchViewModel
import com.bektursun.weatherapp.presentation.search.WeatherSearchViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dataModule = module {
    single { createWeatherApiInstance(androidContext()) }
    single { createFutureWeatherApiInstance(androidContext()) }
    single { createSearchHistoryProvider(androidContext()) }
    single { createSearchHistoryProviderForFuture(androidContext()) }
}

val useCasesModule = module {
    // get — Разрешает компонентные зависимости.
    // Функция сама поймет какая зависимость требуется для каждого класса.
    single { GetCurrentWeatherUseCase(get()) }
    single { GetFutureWeatherUseCase(get()) }
    single { GetLastSearchCityIdUseCase(get()) }
    single { GetLastSearchCityIdFutureForecastUseCase(get()) }
    single { SaveLastSearchCityIdUseCase(get()) }
    single { SaveLastSearchFutureCityIdUseCase(get()) }
    single { MapErrorUseCase() }
}

val viewModelsModule = module {
    // viewModel — Специальное предоставление зависимости для ViewModel,
    // находится в отдельном пакете compile «org.koin:koin-android-architecture:$koin_version»
    viewModel { WeatherSearchViewModel(get(), get(), get(), get()) }
    viewModel { FutureWeatherSearchViewModel(get(), get(), get(), get()) }
    viewModel { (cityId: String) -> CityWeatherDetailsViewModel(cityId, get(), get()) }
    viewModel { (cityId: String) -> FutureCityWeatherDetailsViewModel(cityId, get(), get()) }
}
