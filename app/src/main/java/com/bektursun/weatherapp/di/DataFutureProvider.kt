package com.bektursun.weatherapp.di

import android.content.Context
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.bektursun.weatherapp.BuildConfig
import com.bektursun.weatherapp.repository.net.RemoteFutureDataSource
import com.bektursun.weatherapp.repository.net.interceptors.QueryParamsInterceptor
import com.bektursun.weatherapp.repository.shared_prefs.SearchHistoryProviderForFuture
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

private const val WEATHER_FUTURE_SHARED_PREFS = "future_weather_preferences"

internal fun createSearchHistoryProviderForFuture(context: Context) = SearchHistoryProviderForFuture(context.getSharedPreferences(
    WEATHER_FUTURE_SHARED_PREFS, Context.MODE_PRIVATE))

internal fun createFutureMoshiInstance() = Moshi.Builder()
    .add(Date::class.java, Rfc3339DateJsonAdapter())
    .add(KotlinJsonAdapterFactory())
    .build()

internal fun createFutureWeatherApiInstance(context: Context): RemoteFutureDataSource {

    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    val cacheFile = File(context.cacheDir, "apiResponses")

    val builder = OkHttpClient.Builder()
        .readTimeout(10L, TimeUnit.SECONDS)
        .addInterceptor(QueryParamsInterceptor())
        .cache(Cache(cacheFile, 10 * 1024 * 1024))

    if (BuildConfig.DEBUG) builder.addInterceptor(loggingInterceptor)

    val apiClient = builder.build()

    return Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(createFutureMoshiInstance()).withNullSerialization())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(BuildConfig.API_SERVER)
        .client(apiClient)
        .build()
        .create(RemoteFutureDataSource::class.java)
}