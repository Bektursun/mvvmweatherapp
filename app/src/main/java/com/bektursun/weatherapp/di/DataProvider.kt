package com.bektursun.weatherapp.di

import android.content.Context
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.bektursun.weatherapp.BuildConfig
import com.bektursun.weatherapp.repository.net.RemoteDataSource
import com.bektursun.weatherapp.repository.net.interceptors.QueryParamsInterceptor
import com.bektursun.weatherapp.repository.shared_prefs.SearchHistoryProvider
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

private const val WEATHER_SHARED_PREFS = "weather_preferences"

internal fun createSearchHistoryProvider(context: Context) =
    SearchHistoryProvider(context.getSharedPreferences(WEATHER_SHARED_PREFS, Context.MODE_PRIVATE))


// Создаем объект Moshi
internal fun createMoshiInstance() = Moshi.Builder()
    // парсинг даты
    .add(Date::class.java, Rfc3339DateJsonAdapter())
    .add(KotlinJsonAdapterFactory())
    .build()


internal fun createWeatherApiInstance(context: Context): RemoteDataSource {

    // логирование. Сначала создаем HttpLoggingInterceptor. В нем настраиваем уровень логирования.
    // Если у нас Debug билд, то выставляем максимальный уровень (BODY), иначе - ничего не логируем, чтобы не палить в логах релизные запросы.
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    //  класс File работает не с потоками, а непосредственно с файлами.
    //  Данный класс позволяет получить информацию о файле: права доступа, время и дата создания, путь к каталогу.
    //  А также осуществлять навигацию по иерархиям подкаталогов.
    // parent - The parent abstract pathname,child - The child pathname string
    val cacheFile = File(context.cacheDir, "apiResponses")

    val builder = OkHttpClient.Builder()
        .readTimeout(10L, TimeUnit.SECONDS)
        .addInterceptor(QueryParamsInterceptor())
        .cache(Cache(cacheFile, 10 * 1024 * 1024))

    if (BuildConfig.DEBUG) builder.addInterceptor(loggingInterceptor)

    val apiClient = builder.build()

    return Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(createMoshiInstance()).withNullSerialization())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(BuildConfig.API_SERVER)
        .client(apiClient)
        .build()
        .create(RemoteDataSource::class.java)
}


