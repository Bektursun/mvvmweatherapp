package com.bektursun.weatherapp.domain

class ForecastWeather(
    val unit: Int,
    val temperature: Double,
    val conditionIconRes: Int,
    val day: String
)