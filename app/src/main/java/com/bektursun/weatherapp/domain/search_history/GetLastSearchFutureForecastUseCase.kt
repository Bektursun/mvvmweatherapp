package com.bektursun.weatherapp.domain.search_history

import com.bektursun.weatherapp.repository.shared_prefs.SearchHistoryProviderForFuture
import io.reactivex.Single

class GetLastSearchCityIdFutureForecastUseCase(private val searchHistory: SearchHistoryProviderForFuture) {

    fun executeFuture() = Single.just(searchHistory.lastSearchCityId)

}