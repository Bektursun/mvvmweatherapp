package com.bektursun.weatherapp.domain.search_history

import com.bektursun.weatherapp.repository.shared_prefs.SearchHistoryProviderForFuture
import io.reactivex.Completable

class SaveLastSearchFutureCityIdUseCase(private val searchHistoryProvider: SearchHistoryProviderForFuture) {

    fun executeForFuture(cityId: String) = Completable.create {
        searchHistoryProvider.lastSearchCityId = cityId
        it.onComplete()
    }


}