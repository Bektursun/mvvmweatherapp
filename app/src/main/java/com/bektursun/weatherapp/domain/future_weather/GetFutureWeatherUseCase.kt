package com.bektursun.weatherapp.domain.future_weather

import com.bektursun.weatherapp.domain.toFutureWeatherForecast
import com.bektursun.weatherapp.presentation.FutureWeather
import com.bektursun.weatherapp.repository.net.QueryBuilder
import com.bektursun.weatherapp.repository.net.RemoteFutureDataSource
import io.reactivex.Single

class GetFutureWeatherUseCase(
    private val api: RemoteFutureDataSource
) {

    fun executeFutureForCityName(searchQuery: String): Single<FutureWeather> = api.getFutureForecast(
        QueryBuilder()
            .searchWithCityName(searchQuery)
            .build()
    ).map {
        it.toFutureWeatherForecast()
    }

    fun executeFutureForLocation(lat: Double, lon: Double): Single<FutureWeather> = api.getFutureForecast(
        QueryBuilder()
            .searchWithLocation(lat, lon)
            .build()
    ).map {
        it.toFutureWeatherForecast()
    }

    fun executeFutureForCityId(cityId: String): Single<FutureWeather> = api.getFutureForecast(
        QueryBuilder()
            .searchWithCityId(cityId)
            .build()
    ).map {
        it.toFutureWeatherForecast()
    }
}