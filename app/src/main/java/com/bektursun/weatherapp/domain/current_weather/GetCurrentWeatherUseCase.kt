package com.bektursun.weatherapp.domain.current_weather

import com.bektursun.weatherapp.domain.toCityWeatherForecast
import com.bektursun.weatherapp.presentation.CityCurrentWeather
import com.bektursun.weatherapp.repository.net.QueryBuilder
import com.bektursun.weatherapp.repository.net.RemoteDataSource
import io.reactivex.Single

class GetCurrentWeatherUseCase(
    private val api: RemoteDataSource
) {

    fun executeForCityName(searchQuery: String): Single<CityCurrentWeather> = api.getWeatherForecast(
        QueryBuilder()
            .searchWithCityName(searchQuery)
            .build()
    ).map {
        it.toCityWeatherForecast()
    }

    fun executeForLocation(lat: Double, lon: Double): Single<CityCurrentWeather> = api.getWeatherForecast(
        QueryBuilder()
            .searchWithLocation(lat, lon)
            .build()
    ).map {
        it.toCityWeatherForecast()
    }

    fun executeForCityId(cityId: String): Single<CityCurrentWeather> = api.getWeatherForecast(
        QueryBuilder()
            .searchWithCityId(cityId)
            .build()
    ).map {
        it.toCityWeatherForecast()
    }
}