package com.bektursun.weatherapp.domain.current_weather

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.bektursun.weatherapp.domain.future_weather.GetFutureWeatherUseCase
import com.bektursun.weatherapp.presentation.FutureWeather
import com.bektursun.weatherapp.repository.net.*
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class GetFutureWeatherUseCaseTest {

    private val apiEntityFutureWeatherForecast = FutureWeatherForecast(
        city = (City(Coord(45.33, 32.32), "Kyrgyzstan", 500, "cityName", 600321)),
        cnt = 12,
        cod = "cod",
        list = listOf(X(Clouds(12), dt = 12, dtTxt = "dtTxt",
            main = Main(12.21, 21, 12.22, 12.1, 12.11, 122.3,122.3, 12.2),
            sys = Sys("sdasd"),
            weather = listOf(Weather("description", "icon", 12, "main")),
            wind = Wind(12.22, 212.21)
        )),
        message = 12.2
    )

    private val viewEntityFutureWeather = FutureWeather(
        city = City(Coord(45.33, 32.32), "Kyrgyzstan", 500, "cityName", 600321),
        cnt = 12,
        cod = "cod",
        list = listOf(X(Clouds(12), dt = 12, dtTxt = "dtTxt",
            main = Main(12.21, 21, 12.22, 12.1, 12.11, 122.3,122.3, 12.2),
            sys = Sys("sdasd"),
            weather = listOf(Weather("description", "icon", 12, "main")),
            wind = Wind(12.22, 212.21)
        )),
        message = 12.2
    )

    private val mockApi = mock<RemoteFutureDataSource> {
        on { getFutureForecast(any()) } doReturn Single.just(apiEntityFutureWeatherForecast)
    }

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun makeSchedulersSynchronous() {
        RxJavaPlugins.setInitIoSchedulerHandler { Schedulers.from { command -> command.run() } }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.from { command -> command.run() } }
    }

    @Test
    fun `use case maps correctly api entity to future weather view entity when querying with city id`() {
        val getCurrentFutureWeatherUseCase = GetFutureWeatherUseCase(mockApi)

        val result = getCurrentFutureWeatherUseCase.executeFutureForCityId("500").blockingGet()

        Assert.assertEquals(result, viewEntityFutureWeather)
    }

    @Test
    fun `use case maps correctly api entity to future weather view entity when querying with city name`() {
        // given
        val getCurrentFutureWeatherUseCase = GetFutureWeatherUseCase(mockApi)

        //when
        val result = getCurrentFutureWeatherUseCase.executeFutureForCityName("test name").blockingGet()

        // then)
        Assert.assertEquals(result, viewEntityFutureWeather)
    }

    @Test
    fun `use case maps correctly api entity to future weather view entity when querying with location`() {
        // given
        val getCurrentFutureWeatherUseCase = GetFutureWeatherUseCase(mockApi)

        //when
        val result = getCurrentFutureWeatherUseCase.executeFutureForLocation(11.3, 23.2).blockingGet()

        // then)
        Assert.assertEquals(result, viewEntityFutureWeather)
    }
}